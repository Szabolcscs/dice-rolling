﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class RandomSpawner : MonoBehaviour
{    
    //változók
    public Button Button_Roll;
    public TMP_InputField inp;
    public GameObject[] spawnedObj;
    public int randomInt;

    void Start()
    {
        //hozzáadunk egy listenert a gombhoz, gombnyomásra végrehajtja a függvényt    
       Button_Roll.onClick.AddListener(SpawnRandoms);
     }
    
    //generálunk megadott számú kockát
    void SpawnRandoms()
    {
        //generálunk egy random számot 0-tól 5-ig
        randomInt = Random.Range(0, 6);
        Debug.Log(randomInt);
        Debug.Log("nOD=" + InputOptions.numberOfDices);
        
        //betegeltünk empty gameobjecteket, és beletettünk egy tömbbe, amik helyére fognak menni a klónozott gameobjectek
        GameObject[]places = GameObject.FindGameObjectsWithTag("Place");
        //ha nem állítottuk be a beállításoknál az értéket, akkor 1 lesz, ahány kockát dobunk
        if (InputOptions.numberOfDices==0)
        {
            InputOptions.numberOfDices = 1;
        }
        
        //annyi kockát dobunk amennyit beállítottunk
        for (int i = 0; i < InputOptions.numberOfDices ; i++)
        {
            //klónozunk egy random egy GameObjektet(egy kockát) a tömbből, amit beállítottunk
            GameObject clone = Instantiate(spawnedObj[randomInt]) ;
            
            //a klónnak beállítjuk, hogy a megfelelő empty GameObject legyen a szülője, és, hogy ugyanott legyen, ahol a szülő
            //BUG: az editorban sorban haladnak a megjelenített kockák, de a telepített verzióban alulról kezdődik, és a sorrend nem jó 
            clone.transform.SetParent(places.ElementAt(i).transform, false);
            //generálunk egy random számot 0-tól 5-ig
            randomInt = Random.Range(0, 6);
        }
        //Debug.Log(places.Length);

    }
      
     

    
        
}        
        
        


    

    


