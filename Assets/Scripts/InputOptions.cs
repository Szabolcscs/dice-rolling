﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InputOptions : MonoBehaviour
{
    //változók
    public Button Button_Select;
    public TMP_InputField inputf;
    //statikus változó, a dobandó kockák mennyisége 
    public static int numberOfDices;
    
    void Start()
    {    
        //hozzáadunk egy listenert a gombhoz, gombnyomásra végrehajtja a függvényt    
        Button_Select.onClick.AddListener(DiceNumberAdd);
    }
    //az inputfieldből kiveszi és eltárolja a numberOfDices változóba a beírt értéket
    void DiceNumberAdd()
    {             
            Debug.Log(inputf.text);
            numberOfDices=Int32.Parse(inputf.text);
            //16 a maximum amit meg lehet jeleníteni, ha nagyobb akkor a maximum lesz
            if (numberOfDices > 16)
            {
                numberOfDices = 16;
            }
            Debug.Log(numberOfDices);
    }
}

   

